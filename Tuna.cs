using System;

namespace STVNRLND
{
    class Tuna
    {
        static void Main()
        {
            double totalMinutes = 0;

            // ------ Repeat until user quits
            while (true)
            {
                string entry;
                double minutesEntered;

                // ------ Prompt user for minutes worked
                Console.Write("How many minutes have you been working? Type \"quit\" to exit.");
                entry = Console.ReadLine();

                if (entry.ToLower() == "quit")
                {
                    break;
                }

                try
                {
                    minutesEntered = double.Parse(entry);

                    if (minutesEntered <= 0)
                    {
                        Console.WriteLine(minutesEntered + " is not an acceptable entry!");
                        continue;
                    }
                    else if (minutesEntered <= 10)
                    {
                        Console.WriteLine("Good job!");
                    }
                    else if (minutesEntered <= 30)
                    {
                        Console.WriteLine("Fantastic work!");
                    }
                    else if (minutesEntered <= 60)
                    {
                        Console.WriteLine("Whoa, that's awesome!");
                    }
                    else
                    {
                        Console.WriteLine("You've been working for a long time, maybe it's time for a short break!");
                    }

                    // ------ Add minutes worked to the running total
                    totalMinutes += minutesEntered;

                    // ------ Dsiplay updated total to the user
                    Console.WriteLine("You've been working for " + totalMinutes + " minutes.");
                }
                catch (FormatException)
                {
                    Console.WriteLine("That is not valid input.");
                    continue;
                }
            }
            Console.WriteLine("Goodbye!");
        }
    }
}